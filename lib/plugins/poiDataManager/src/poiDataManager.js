import * as R from 'ramda';
import Zousan from 'zousan';
import { buildStructuresLookup } from '../../../src/utils/buildStructureLookup.js';
import { debugIsTrue } from '../../../src/utils/configUtils.js';
import { toLang } from '../../../src/utils/i18n.js';

async function create (app, config) {
  const init = () => {
    app.bus.send('venueData/loadPoiData');
  };

  let poisLoaded = new Zousan();

  const fixPositionInfo = (poi, structuresLookup) => {
    const { position } = poi;
    const structure = structuresLookup.floorIdToStructure(position.floorId);
    if (!structure) {
      console.error(`No structure found for floorId: ${position.floorId} for POI ${poi.poiId}`);
      return { ...poi }
    }
    const floor = structuresLookup.floorIdToFloor(position.floorId);
    const detailedPosition = {
      ...position,
      structureName: structure.name,
      buildingId: structure.id,
      floorName: floor.name,
      floorOrdinal: floor.ordinal
    };
    return { ...poi, position: detailedPosition }
  };

  // todo R.map may be enough to update dictionary values
  const formatPois = (pois, structuresLookup) => {
    return R.pipe(
      R.values,
      R.map(poi => {
        poi.distance = null;
        poi.isNavigable = poi.isNavigable === undefined || poi.isNavigable === true; // isNavigable is true as default, and is optional in the POI data
        // poi.isNavigable = /^[a-mA-M]+/.test(poi.name) // uncomment for easy testing of isNavigable
        if (poi.capacity)
          addToRoomInfo(poi, { name: `Seats ${poi.capacity.join('-')}`, svgId: 'number-of-seats' });
        if (poi.category.startsWith('meeting'))
          addToRoomInfo(poi, { name: app.gt()('poiView:Conference Room'), svgId: 'conference-room' });

        const roomId = getRoomId(poi);
        if (roomId)
          poi.roomId = roomId;

        return [poi.poiId, fixPositionInfo(poi, structuresLookup)]
      }),
      R.fromPairs
    )(pois)
  };

  const addToRoomInfo = (poi, value) => {
    if (!poi.roomInfo) {
      poi.roomInfo = [];
    }
    poi.roomInfo.push(value);
  };

  const getRoomId = R.pipe(
    R.propOr([], 'externalIds'),
    R.find(R.propEq('type', 'roomId')),
    R.prop('id'),
    R.unless(R.isNil, R.tail)
  );

  app.bus.on('venueData/poiDataLoaded', ({ pois, structures }) => {
    const structuresLookup = buildStructuresLookup(structures);
    const formattedPois = formatPois(pois, structuresLookup);
    if (debugIsTrue(app, 'pseudoTransPois'))
      for (const id in formattedPois)
        formattedPois[id] = pseudoTransPoi(formattedPois[id], app.i18n().language);
    poisLoaded.resolve(formattedPois);

    if (app.config.debug && app.env.isBrowser)
      window._pois = formattedPois;
  });

  app.bus.on('poi/getById', async ({ id }) => {
    return poisLoaded.then(pois => pois[id]).then(poi => poi && addImages(poi))
  });

  app.bus.on('poi/getByFloorId', async ({ floorId }) => poisLoaded.then(
    R.pickBy(R.pathEq(['position', 'floorId'], floorId))));

  app.bus.on('poi/getByCategoryId', async ({ categoryId }) => {
    // returns true for exact category matches or parent category matches
    const categoryMatch = (poi) => poi.category === categoryId || poi.category.startsWith(categoryId + '.');
    return poisLoaded.then(R.pickBy(categoryMatch))
  });

  app.bus.on('poi/getAll', async () => {
    return poisLoaded
  });

  const checkpointPath = ['queue', 'primaryQueueId'];
  const addOtherSecurityLanesIfNeeded = async poi => {
    const primaryCheckpointId = R.path(checkpointPath, poi);

    if (!primaryCheckpointId) return poi

    const queueTypes = await app.bus.get('venueData/getQueueTypes');
    const securityPoisMap = await app.bus.get('poi/getByCategoryId', { categoryId: 'security' });
    const securityPoisList = Object.values(securityPoisMap);

    poi.queue.otherQueues = prepareOtherSecurityLanes(queueTypes, poi, securityPoisList);
    return poi
  };

  const prepareOtherSecurityLanes = (queueTypes, currentPoi, securityPois) => {
    const queueTypePath = ['queue', 'queueType'];
    const queueType = R.path(queueTypePath, currentPoi);
    if (!queueType)
      return null

    const queueSubtypes = queueTypes[queueType];
    const primaryCheckpointId = R.path(checkpointPath, currentPoi);
    return securityPois
      .filter(R.pathEq(checkpointPath, primaryCheckpointId)) // filter only connected security checkpoints
      .filter(poi => poi.poiId !== currentPoi.poiId) // skip current poi
      .map(poi => {
        const laneId = R.path(['queue', 'queueSubtype'], poi);
        const lane = getLaneData(laneId)(queueSubtypes);
        return { poiId: poi.poiId, ...lane }
      })
  };

  const getLaneData = laneId => R.pipe(
    R.find(R.propEq('id', laneId)),
    R.pick(['displayText', 'imageId'])
  );

  /**
   * Updates security checkpoint POI with a list of related security checkpoints.
   */
  app.bus.on('poi/addOtherSecurityLanes', ({ poi }) => addOtherSecurityLanesIfNeeded(poi));

  async function addImages (poi) {
    if (!poi) return
    if (!R.length(poi.images)) {
      poi.images = [];
    } else if (!poi.images[0].startsWith('https:')) { // then images are not yet transformed
      poi.images = await Zousan.all(
        poi.images.map(imageName =>
          app.bus.get('venueData/getPoiImageUrl', { imageName, size: '480x320' })));
    }
    return poi
  }

  const getUniqueCategories = R.memoizeWith(R.identity, R.pipe(R.pluck('category'), R.values, R.uniq));
  app.bus.on('poi/getAllCategories', async () => poisLoaded.then(getUniqueCategories));

  app.bus.on('venueData/loadNewVenue', () => {
    poisLoaded = new Zousan();
    init();
  });

  // See architectural document at  https://docs.google.com/document/d/1NoBAboHR9BiX_vvLef-vp3ButrIQDWYofcTsdilEWvs/edit#
  app.bus.on('poi/setDynamicData', ({ plugin, idValuesMap }) => {
    poisLoaded
      .then(pois => {
        for (const poiId in idValuesMap) {
          // const dd = { [plugin]: idValuesMap[poiId] }
          const dynamicData = pois[poiId].dynamicData || {};
          dynamicData[plugin] = { ...idValuesMap[poiId] };
          const newPoi = R.mergeRight(pois[poiId], { dynamicData });
          pois[poiId] = newPoi;
        }
      });
  });

  const runTest = async (testRoutine) => {
    await testRoutine();
    return poisLoaded
  };

  return {
    init,
    runTest,
    internal: {
      addImages,
      pseudoTransPoi
    }
  }
}

function pseudoTransPoi (poi, lang) {
  ['description', 'nearbyLandmark', 'name', 'phone', 'operationHours']
    .forEach(p => {
      if (poi[p])
        poi[p] = toLang(poi[p], lang);
    });
  if (poi.keywords)
    poi.keywords = poi.keywords.map(keyword => {
      keyword.name = toLang(keyword.name, lang);
      return keyword
    });
  if (poi.position.floorName)
    poi.position.floorName = toLang(poi.position.floorName, lang);
  if (poi.position.structureName)
    poi.position.structureName = toLang(poi.position.structureName, lang);
  return poi
}

export { create };
