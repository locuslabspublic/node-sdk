import * as R from 'ramda';
import Zousan from 'zousan';
import { buildStructuresLookup } from '../../../src/utils/buildStructureLookup.js';
import { distance } from '../../../src/utils/geodesy.js';
import { findRoute } from './findRoute.js';
import { createNavGraph } from './navGraph.js';
import { enrichDebugNavGraph } from './navGraphDebug.js';
import { buildSegments } from './segmentBuilder.js';

const DEFAULT_WALKING_SPEED_M_PER_MIN = 60;

const getEdgeTo = dst => node => R.find(e => e.dst === dst, node.edges);

// todo may be not needed
const SecurityLaneType = {
  SECURITY: 'SecurityLane',
  IMMIGRATION: 'ImmigrationLane'
};

/**
 * @typedef {Object} Endpoint
 * @property {number} lat - latitude
 * @property {number} lng - longitude
 * @property {string} title
 * @property {string} [floorId] - usually present
 * @property {number} [ordinal] - optional
 *
 * @typedef SecurityLaneIdsMap
 * @property {string[]} SecurityLane - list of ids of security lanes
 * @property {string[]} ImmigrationLane - list of ids of immigration lanes
 *
 * @typedef Route
 * @property {Step[]} steps - list of navigation steps
 * @property {Segment[]} segments - list of navigation line segments
 * @property {number} time - total route time
 * @property {number} distance - total route distance
 *
 * @typedef RouteOptions
 * @property {SecurityLaneIdsMap} selectedSecurityLanes - map of selected lane ids by type
 * @property {boolean} requiresAccessibility - true if route should be accessible
 * @property {boolean} compareFindPaths - indicate whether to calculate path using 2 methods and then compare their performance
 *
 * @typedef SecurityWaitTime
 * @property {number} queueTime
 * @property {boolean} timeIsReal
 * @property {boolean} isTemporarilyClosed
 *
 * @typedef {Array<number>} Coordinate - pair of lng and lat
 *
 */
function create (app, config) {
  const log = app.log.sublog('wayfinder');
  const init = async () => {
    app.bus.send('venueData/loadNavGraph');
  };

  let graphLoadedProm = new Zousan();

  /**
   * Returns nav graph object for testing purposes.
   * Result includes nav nodes, edges, functions to update dynamic data and calculate shortest paths
   *
   * @returns {Object}
   */
  app.bus.on('wayfinder/_getNavGraph', () => graphLoadedProm);

  /**
   * @typedef RawNavGraph
   * @property Array.<RawNavEdge> edges
   * @property Array.<RawNavNode> nodes
   *
   * @typedef RawNavEdge
   * @property {string} s - id of start node
   * @property {string} d - id of destination node
   * @property {number} l - custom transit time
   * @property {boolean} h - is edge a driveway
   * @property {string} t - edge type
   * @property {Array.<{ s, o, i, e }>|null} p - list of Bezier points
   *
   * @typedef RawNavNode
   * @property {string} id
   * @property {string} floorId
   * @property {number} lat
   * @property {number} lng
   *
   * Transforms raw nav graph data and list of structures
   * to nav graph object with functions to build shortest paths
   *
   * @param {RawNavGraph} navGraphData
   * @param {Array.<Structure>} structures
   */
  app.bus.on('venueData/navGraphLoaded', async ({ navGraphData, structures }) => {
    const structureLookup = buildStructuresLookup(structures);
    const securityLanesMap = await prepareSecurityLanes();
    const graph = createNavGraph(
      navGraphData,
      structureLookup.floorIdToOrdinal,
      structureLookup.floorIdToStructureId,
      securityLanesMap
    );
    graphLoadedProm.resolve(graph);
  });

  const prepareSecurityLanes = async () => {
    const securityPois = await app.bus.get('poi/getByCategoryId', { categoryId: 'security' });
    return R.pipe(R.map(getSecurityLane), R.filter(R.identity))(securityPois)
  };

  const getSecurityLane = poi => poi.queue && {
    type: R.path(['queue', 'queueType'], poi),
    id: R.path(['queue', 'queueSubtype'], poi)
  };

  /**
   * Returns a shortest path from user physical location to provided destination
   * and triggers rendering navigation line on the map
   *
   * @param {Endpoint} toEndpoint
   * @param {Boolean} requiresAccessibility
   * @param {SecurityLaneIdsMap} selectedSecurityLanes
   * @returns {Route}
   */
  app.bus.on('wayfinder/showNavLineFromPhysicalLocation', async ({ toEndpoint, selectedSecurityLanes = null, requiresAccessibility }) => {
    const physicalLocation = await app.bus.get('user/getPhysicalLocation');
    return navigateFromTo(physicalLocation, toEndpoint, { selectedSecurityLanes, requiresAccessibility, primary: true })
  });

  async function navigateFromTo (fromEndpoint, toEndpoint, options) {
    const route = await getRoute({ fromEndpoint, toEndpoint, options });
    if (route) {
      const { segments } = route;
      if (options.primary)
        app.bus.send('map/resetNavlineFeatures');
      app.bus.send('map/showNavlineFeatures', { segments, alternative: !options.primary });
    }

    return route
  }

  const poiIdToNavigationEndpoint = (id, floorIdToOrdinal) =>
    app.bus.get('poi/getById', { id })
      .then(poi => {
        if (poi && poi.position) {
          return poiToNavigationEndpoint(poi, floorIdToOrdinal)
        } else
          throw Error('Unknown POI ID ' + id)
      });

  /**
   * @busEvent wayfinder/getNavigationEndpoint
   *
   * Returns an object of the Endoint type.
   * wayfinding uses this structure to find the closest node
   * for shortestPath calculations, etc.
   * @param  {Object} p - can be a POI (or similar) or a string with lat,lng[,floorId[,name]] or location defined as { latitutde, longitude [, floorId] [, title] }
   * @returns {Endpoint} navigational endpoint
   */
  async function getNavigationEndpoint (p) {
    return graphLoadedProm.then(graph => {
      if (!p)
        throw Error('wayfinder: Invalid endpoint definition', p)

      if (typeof p === 'number')
        return poiIdToNavigationEndpoint(p, graph.floorIdToOrdinal)

      if (typeof p === 'string') {
        if (p.match(/^\d+$/)) // single integer - assume its poi id
          return poiIdToNavigationEndpoint(parseInt(p), graph.floorIdToOrdinal)

        if (p.indexOf(',') > 0) { // lat,lng,floorId,desc format
          let [lat, lng, floorId, title] = p.split(',');
          if (!graph.floorIdToStructureId(floorId))
            throw Error('Unknown floorId in endpoint: ' + floorId)
          if (!title)
            title = 'Starting Point';

          return {
            lat: parseFloat(lat),
            lng: parseFloat(lng),
            ordinal: graph.floorIdToOrdinal(floorId),
            floorId,
            title
          }
        }
      }

      if (isEndpoint(p))
        return p

      if (p.latitude)
        return {
          lat: p.latitude,
          lng: p.longitude,
          floorId: p.floorId,
          ordinal: graph.floorIdToOrdinal(p.floorId),
          title: p.title
        }

      if (p.position && p.name) // looks like a POI or some other
        return poiToNavigationEndpoint(p, graph.floorIdToOrdinal)

      throw Error('Invalid start or end point: ' + p)
    })
  }

  const endpointProps = ['lat', 'lng', 'floorId', 'ordinal'];
  const isEndpoint = R.pipe(
    R.pick(endpointProps),
    R.keys,
    R.propEq('length', endpointProps.length),
    Boolean
  );

  const poiToNavigationEndpoint = (poi, floorIdToOrdinal) => ({
    lat: poi.position.latitude,
    lng: poi.position.longitude,
    floorId: poi.position.floorId,
    ordinal: floorIdToOrdinal(poi.position.floorId),
    title: poi.name
  });

  /**
   * Transforms provided data to endpoint type object
   *
   * @return {Endpoint} - navigation endpoint
   */
  app.bus.on('wayfinder/getNavigationEndpoint', ({ ep }) => getNavigationEndpoint(ep));

  /**
   * @typedef PathSecurityInfo
   * @property {boolean} routeExists
   * @property {boolean} [hasSecurity]
   * @property {boolean} [hasImmigration]
   *
   * Checks if there is a path between 2 endpoints which satisfies passed options
   * and if this path includes security and immigration lanes
   *
   * @param {Endpoint} fromEndpoint
   * @param {Endpoint} toEndpoint
   * @param {RouteOptions} options
   * @returns {PathSecurityInfo}
   */
  app.bus.on('wayfinder/checkIfPathHasSecurity', ({ fromEndpoint, toEndpoint, options = {} }) => graphLoadedProm
    .then(graph => {
      options.compareFindPaths = config.compareFindPaths;
      const route = findRoute(graph, fromEndpoint, toEndpoint, options);

      if (!route) return { routeExists: false }

      const queues = route.waypoints
        .filter(node =>
          R.pathEq(['securityLane', 'type'], SecurityLaneType.SECURITY, node) ||
          R.pathEq(['securityLane', 'type'], SecurityLaneType.IMMIGRATION, node));
      const containsSecurityLaneType = type => Boolean(route.waypoints.find(R.pathEq(['securityLane', 'type'], type)));
      return {
        routeExists: true,
        queues,
        hasSecurity: containsSecurityLaneType(SecurityLaneType.SECURITY),
        hasImmigration: containsSecurityLaneType(SecurityLaneType.IMMIGRATION)
      }
    }));

  app.bus.on('wayfinder/getRoute', getRoute);

  /**
   * @busEvent wayfinder/getRoute
   *
   * Builds the shortest path between 2 endpoints which satisfies passed options
   *
   * @param {RouteOptions} options
   * @param {Endpoint} fromEndpoint
   * @param {Endpoint} toEndpoint
   *
   * @return {(Route|null)} route - route or null if no route available
   */
  async function getRoute ({ fromEndpoint, toEndpoint, options = {} }) {
    return graphLoadedProm
      .then(async graph => {
        options.compareFindPaths = config.compareFindPaths;
        const route = findRoute(graph, fromEndpoint, toEndpoint, options);
        if (!route) return null
        sendRouteAnalytic(fromEndpoint, toEndpoint, route); // todo move to analytics (NOTE: we call this twice for each nav, doubling stats!)
        const floorIdToNameMap = await app.bus.get('venueData/getFloorIdToNameMap');
        const queueTypes = await app.bus.get('venueData/getQueueTypes');
        const translate = app.gt();
        const { steps, segments } = buildSegments(
          route.waypoints,
          fromEndpoint,
          toEndpoint,
          floorIdToNameMap,
          translate,
          queueTypes);

        log.info('route', route);
        const time = Math.round(route.waypoints.reduce((total, { eta }) => total + eta, 0));
        const distance = Math.round(route.waypoints.reduce((total, { distance }) => total + distance, 0));
        return { ...route, segments, steps, time, distance }
      })
  }

  const sendRouteAnalytic = (start, end, navigationPath) => app.bus.send('session/submitEvent', {
    type: 'navigation',
    startPosition: {
      venueId: start.floorId.split('-')[0],
      buildingId: navigationPath.waypoints[0].position.structureId,
      floorId: navigationPath.waypoints[0].position.floorId,
      lat: navigationPath.waypoints[0].position.lat,
      lng: navigationPath.waypoints[0].position.lng
    },
    endPosition: {
      venueId: end.floorId.split('-')[0],
      buildingId: navigationPath.waypoints[navigationPath.waypoints.length - 1].position.structureId,
      floorId: navigationPath.waypoints[navigationPath.waypoints.length - 1].position.floorId,
      lat: navigationPath.waypoints[navigationPath.waypoints.length - 1].position.lat,
      lng: navigationPath.waypoints[navigationPath.waypoints.length - 1].position.lng
    }
  });

  /**
   * Calculates transit time and distance of shortest path to start location which satisfies passed options
   * and returns copy of POI with these new properties
   *
   * @param {Endpoint} startLocation
   * @param {RouteOptions} options
   * @returns {Object} - POI
   */
  app.bus.on('wayfinder/addPathTimeSingle', async ({ poi, startLocation, options = {} }) => {
    if (!startLocation) return poi
    return graphLoadedProm.then(graph => addPathTimeSingle(graph, options, poi, startLocation))
  });

  function addPathTimeSingle (graph, options, poi, start) {
    const end = poiToNavigationEndpoint(poi, graph.floorIdToOrdinal);

    const path = graph.findShortestPath(start, end, options);

    if (!path) {
      poi = getPoiWithGeoDistance(poi, start);
      poi.transitTime = poi.distance / DEFAULT_WALKING_SPEED_M_PER_MIN;
      return poi
    }
    return Object.assign(poi, { transitTime: pathTime(path), distance: pathDistance(path) })
  }

  /**
   * Calculates transit time and distance of shortest path from each POI to start location which satisfies passed options
   * and returns list of copies of POI with these new properties
   *
   * @param {Endpoint} startLocation
   * @param {RouteOptions} options
   * @param pois: array of pois
   * @returns Array.<Object> - list of POIs
   */
  app.bus.on('wayfinder/addPathTimeMultiple', async ({ pois, startLocation, options = {} }) => {
    if (!startLocation) return pois
    return graphLoadedProm.then(graph => addPathTimeMultiple(graph, options, pois, startLocation))
  });

  function addPathTimeMultiple (graph, options, pois, start) {
    const poiLocations = pois.map(poi => poiToNavigationEndpoint(poi, graph.floorIdToOrdinal));
    const paths = graph.findAllShortestPaths(start, poiLocations, options);
    return pois
      .map((poi, i) => resolveAndAddPathProps(poi, paths[i], start))
  }

  function resolveAndAddPathProps (poi, path, startLocation) {
    if (path && path.length) {
      poi = Object.assign(poi, { transitTime: pathTime(path), distance: pathDistance(path) });
    } else {
      poi = getPoiWithGeoDistance(poi, startLocation);
      poi.transitTime = poi.distance / DEFAULT_WALKING_SPEED_M_PER_MIN;
    }
    return poi
  }

  function pathTime (path) {
    return calculateTotalPathProperty(path, 'transitTime')
  }

  function pathDistance (path) {
    return calculateTotalPathProperty(path, 'distance')
  }

  function calculateTotalPathProperty (path, propertyName) {
    return R.aperture(2, path)
      .map(([from, to]) => getEdgeTo(to.id)(from))
      .map(R.prop(propertyName))
      .reduce((totalTime, edgeTime) => totalTime + edgeTime, 0)
  }

  function getPoiWithGeoDistance (poi, startLocation) {
    // todo should transitTime and distance be root poi properties?
    poi.distance = distance(
      startLocation.lat, startLocation.lng,
      poi.position.latitude, poi.position.longitude);
    return poi
  }

  /**
   * Resets plugin state
   */
  app.bus.on('venueData/loadNewVenue', () => {
    graphLoadedProm = new Zousan();
    init();
  });

  /**
   * Updates nav graph dynamic data if security data is passed
   *
   * @param {string} plugin - type of dynamic data
   * @param {Object<string, SecurityWaitTime|Object>} - dictionary of POI id to dynamic data object
   */
  app.bus.on('poi/setDynamicData', ({ plugin, idValuesMap }) => {
    if (plugin !== 'security') return
    graphLoadedProm.then(graph => graph.updateWithSecurityWaitTime(idValuesMap));
  });

  /**
   * Returns a list of edges and nodes in a format convenient to display them on the map
   *
   * @typedef DebugNode
   * @property {string} floorId
   * @property {string} id
   * @property {boolean} isOrphaned
   * @property {number} lat
   * @property {number} lng
   * @property {number} ordinal
   * @property {string} structureId
   *
   * @typedef DebugEdge
   * @property {Coordinate} startCoordinates
   * @property {Coordinate} endCoordinates
   * @property {boolean} isDriveway
   * @property {number} ordinal
   * @property {string} category
   * @property {string} defaultStrokeColor
   *
   * @returns {{nodes: DebugNode[], edges: DebugEdge[]}} debug nav graph
   */
  app.bus.on('wayfinder/getNavGraphFeatures', () => graphLoadedProm
    .then(({ _nodes }) => enrichDebugNavGraph(_nodes)));

  return {
    init,
    internal: {
      resolveNavGraph: graph => graphLoadedProm.resolve(graph),
      prepareSecurityLanes
    }
  }
}

export { SecurityLaneType, create };
