import * as R from 'ramda';
import { locationToEndpoint, getStructures } from './util.js';

const headlessCommands = [
  {
    command: 'getDirections',
    args: [
      { name: 'from', type: 'location' },
      { name: 'to', type: 'location' },
      { name: 'accessible', type: 'boolean', optional: true }
    ]
  },
  { command: 'getPOIDetails', args: [{ name: 'poiId', type: 'integer', min: 0 }] },
  { command: 'getAllPOIs' },
  { command: 'getStructures' },
  { command: 'getVenueData' },
  {
    command: 'search',
    args: [
      { name: 'term', type: 'string', minLength: 2 },
      { name: 'details', type: 'boolean', optional: true }
    ]
  }
];

function handleHeadless (app) {
  app.bus.on('clientAPI/getDirections', async ({ from, to, accessible }) => {
    const fromEndpoint = await locationToEndpoint(app, from);
    const toEndpoint = await locationToEndpoint(app, to);
    return app.bus.get('wayfinder/getRoute', { fromEndpoint, toEndpoint, options: { requiresAccessibility: accessible } })
      .then(R.pick(['distance', 'time', 'steps', 'navline']))
  });

  app.bus.on('clientAPI/getPOIDetails', async ({ poiId }) => app.bus.get('poi/getById', { id: poiId }));

  app.bus.on('clientAPI/getAllPOIs', async () => app.bus.get('poi/getAll'));

  app.bus.on('clientAPI/getStructures', () => getStructures(app));

  const isNotFunction = o => typeof o !== 'function';
  app.bus.on('clientAPI/getVenueData', async () => {
    const vd = await app.bus.get('venueData/getVenueData');
    return R.filter(isNotFunction, vd)
  });

  app.bus.on('clientAPI/search', async ({ term, details }) => app.bus.get('search/queryAsync', { term }).then(poiList => {
    const poiIdList = poiList.map(poi => poi.poiId);
    app.bus.send('event/search', { referrer: 'prog', searchMethod: null, query: term, entities: poiIdList });
    return details ? poiList : poiIdList
  }));
}

export { handleHeadless, headlessCommands };
