import { clearCommands, registerCommand, registerCustomType, execute } from '../../../src/extModules/flexapi/src/index.js';

function create (app, config) {
  clearCommands();
  app.bus.on('clientAPI/registerCommand', ob => registerCommand(ob, cob => app.bus.get(`clientAPI/${cob.command}`, cob)));
  app.bus.on('clientAPI/registerCustomType', ({ name, spec }) => registerCustomType(name, spec));
  app.bus.on('clientAPI/execute', execute);

  return {
    init: () => {} // nothing more to do
  }
}

export { create };
