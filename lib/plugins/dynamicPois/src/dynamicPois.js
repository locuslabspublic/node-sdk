import * as R from 'ramda';
import Zousan from 'zousan';

/*
    This service obtains dynamic POI data from our own backend REST API service.
    Currently it drives dynamic data for Security Wait Times and Parking Status.

    Configuration Properties Recognized:
      urlBase : to override the REST url base. i.e. for alpha use 'https://rest-alpha.locuslabs.com/v1'. You can also reference
          a local file, such as './testDynamicPois.json'

    Values defined by 'parking':
      lotName: Name of the lot for this POI, such as 'East'
      lotStatus: returns either 'Closed' or 'Open'
      timeIsReal: If true, the time here is live and valid, else its a default fallback
      rateDay: Daily Rate expressed in English: '$17 per day'
      rateHour: Hourly Rate expressed in English: '$5 per hour'
      timeToTerminal1: Time in walking or by shuttle to Terminal 1 : 'Shuttle 5-7'
      timeToTerminal2: Time in nwalking or by shuttle to Terminal 2 : 'Walking 5-10'
      lastUpdated: timestamp of last update (as sent by server)

    Values defined by 'security'
      queueTime: Estimated Time in queue (in minutes) : i.e. 45
      isTemporarilyClosed: If true, this line is closed - else it is open
      timeIsReal: If true, the time here is live and valid, else its a default fallback
      lastUpdated: timestamp of last update (as sent by server)
 */

const REFRESH_FREQUENCY = 1000 * 30; // every 30 seconds

function create (app, config) {
  let dataLoadedProm = new Zousan();

  const init = async () => {
    const urlBaseNew = config.urlBase || 'https://rest.locuslabs.com/v3';
    const urlBase = config.urlBase || 'https://rest.locuslabs.com/v1';

    async function getURL () {
      return dataLoadedProm
        .then(({ venueId, accountId }) => {
          let url = `${urlBase}/venue/${venueId}/account/${accountId}/get-all-dynamic-pois/`;
          if (urlBase.startsWith('./') || urlBase.endsWith('.json'))
            url = urlBase;
          return url
        })
    }

    /*
      API URL: https://gitlab.com/locuslabs/core-data-team/rest-api/-/blob/develop/v3/docs/REST%20API%20v3.postman_collection.json
     */
    const getWaitTimesUrl = async () => dataLoadedProm.then(({ venueId, accountId }) =>
      (urlBaseNew.startsWith('./') || urlBaseNew.endsWith('.json'))
        ? urlBaseNew
        : `${urlBaseNew}/venueId/${venueId}/accountId/${accountId}/get-dynamic-queue-times/`);

    const updateFromAPI = async () => {
      getURL()
        .then(fetch)
        .then(r => r.json())
        .then(poiMap => processParkingPOIS(Date.now(), poiMap));
      getWaitTimesUrl()
        .then(fetch)
        .then(r => r.json())
        .then(waitTimes => processSecurityWaitTimes(Date.now(), waitTimes));
    };

    // Currently, the only way to know if a venue has dynamic POIs is if they have security wait times
    // and the only way we know that is if they have queueTypes.
    // I know this sounds "fragile" - but Jessica said it is our current truth.
    // This will certainly need to change at some point.
    const queueTypes = await app.bus.get('venueData/getQueueTypes');
    if (queueTypes.SecurityLane && queueTypes.SecurityLane.length)
      dataLoadedProm
        .then(updateFromAPI)
        .then(() => setInterval(updateFromAPI, REFRESH_FREQUENCY));
  };

  function processParkingPOIS (timeNowMs, poiMap) {
    const idValuesMap =
      R.pipe(
        R.filter(poi => poi.category === 'parking'),
        R.map(poi => {
          const da = poi.dynamicAttributes;
          if (!da)
            throw Error(`No dynamicAttributes defined for parking POI ${poi.poiId}`)
          const age = (timeNowMs - poi.timestamp) / 1000; // how long ago this was updated by backend (in seconds)
          const props = (age < da['parking.timeToLive']) // if this update is recent enough, consider it "valid"
            ? R.pick(['lotStatus', 'rateDay', 'rateHour', 'timeIsReal', 'timeToTerminal1', 'timeToTerminal2'], poi)
            : { lotStatus: da['parking.default'], rateDay: '$ -', rateHour: '$ -', timeIsReal: false };

          return { ...props, lastUpdated: poi.timestamp, lotName: poi.lotName }
        }))(poiMap);

    app.bus.send('poi/setDynamicData', { plugin: 'parking', idValuesMap });
  }

  /*
    API response: https://gitlab.com/locuslabs/core-data-team/json-schemas/-/blob/develop/src/api-marketplace/dynamic-queue-data.json
   */
  function processSecurityWaitTimes (timeNowMs, waitTimes) {
    const idValuesMap = R.pipe(
      R.map(waitTime => [waitTime.poiId, toDynamicWaitTime(timeNowMs, waitTime)]),
      R.fromPairs
    )(waitTimes);
    app.bus.send('poi/setDynamicData', { plugin: 'security', idValuesMap });
  }

  const toDynamicWaitTime = (timeNowMs, waitTime) => ({
    queueTime: waitTime.queueTime,
    isTemporarilyClosed: waitTime.isTemporarilyClosed,
    timeIsReal: !waitTime.isQueueTimeDefault && waitTime.expiration > timeNowMs,
    lastUpdated: timeNowMs
  });

  app.bus.on('venueData/mapDataLoaded', ({ venueId, accountId }) => {
    if (dataLoadedProm.v) // non-standard - indicates promise has been resolved...
      dataLoadedProm = Zousan.resolve({ venueId, accountId });
    else
      dataLoadedProm.resolve({ venueId, accountId });
  });

  return {
    init,
    internal: {
      processSecurityWaitTimes,
      processParkingPOIS
    }
  }
}

export { create };
