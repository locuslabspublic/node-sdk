import FlexSearch from 'flexsearch';

const NON_ASCII_LANGUAGES = ['ko', 'ja', 'zh-Hans', 'zh-Hant'];

const standardTokenizer = {
  profile: 'match'
};

const typeaheadTokenizer = {
  encode: 'advanced'
};

const fullTokenizer = {
  encode: false,
  split: /\s+/,
  tokenize: 'full'
};

const TOKENIZERS = {
  standard: standardTokenizer,
  typeahead: typeaheadTokenizer,
  full: fullTokenizer
};

const getFlexSearchInstance = ({ lang, type = 'standard' }) => {
  const tokenizer = NON_ASCII_LANGUAGES.includes(lang) ? fullTokenizer : TOKENIZERS[type];
  return new FlexSearch({ ...tokenizer })
};

export { getFlexSearchInstance };
