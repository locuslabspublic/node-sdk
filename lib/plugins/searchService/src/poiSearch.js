import * as R from 'ramda';
import { getFlexSearchInstance } from './utils.js';

function createPOISearch (pois, lang) {
  const index = getFlexSearchInstance({ lang });

  index.addMatcher({
    '[\'.,]': ''
  });

  prepareIndexEntries(pois)
    .forEach(([id, content]) => index.add(id, content));

  function prepareIndexEntries (pois) {
    return Object.values(pois).map((poi) => {
      const { poiId, category = '', name, keywords = [], roomId = '' } = poi;
      const grabTags = R.path(['dynamicData', 'grab', 'tags'], poi) || [];
      const searchKeywords = keywords
        .filter(R.prop('isUserSearchable'))
        .map(R.prop('name'));
      const content = `${name} ${category.split('.').join(' ')} ${roomId} ${searchKeywords.join(' ')} ${grabTags.join(' ')}`;
      return [Number(poiId), content]
    })
  }

  function search (queryParams) {
    const ids = index.search(queryParams);
    return Object.values(R.pick(ids, pois))
  }

  function updateMultiple (pois) {
    prepareIndexEntries(pois)
      .forEach(([id, content]) => index.update(id, content));
  }

  return { search, updateMultiple }
}

export default createPOISearch;
