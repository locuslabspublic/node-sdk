import * as R from 'ramda';
import { findBoundsOfCoordinates } from '../../../src/utils/bounds.js';

const fetchURL = async (token, url) => {
  if (token) {
    return fetch(url, {
      headers: {
        Authorization: token
      }
    })
  } else
    return fetch(url)
};

const createFetchJson = (token) => url => fetchURL(token, url).then(response => response.json());
const createFetchText = (token) => url => fetchURL(token, url).then(response => response.text());

const baseContentUrl = (contentStage) => `https://api.content.locuslabs.com/${contentStage}`;

const remapContentUrl = (url, category, contentStage, accountId, venueId, key) => {
  if (key === 'theme' || key === 'style') {
    return `${baseContentUrl(contentStage)}/${category}/${key}/${venueId}/${accountId}/${key}.json`
  }
  return url.replace(/https:\/\/content.locuslabs.com/gi, baseContentUrl(contentStage))
};

const remapEachURLForContentStage = (category, files, contentStage, accountId, venueId) =>
  R.mapObjIndexed((file, key) => remapContentUrl(file, category, contentStage, accountId, venueId, key), files);

const getContentStage = (vconfig) => {
  const stage = vconfig.deepLinkProps ? vconfig.deepLinkProps.contentStage : null;
  return stage === 'alpha' || stage === 'beta' || stage === 'prod' ? stage : null
};

const getVenueDataFromUrls = async (vconfig, fetchJson) => {
  const stages = {
    alpha: 'alpha-a.locuslabs.com',
    beta: 'beta-a.locuslabs.com',
    gamma: 'gamma-a.locuslabs.com',
    prod: 'a.locuslabs.com'
  };
  const { assetStage, venueId, accountId, formatVersion } = vconfig;
  const stageUrl = stages[assetStage] || stages.prod;
  const accountUrl = `https://${stageUrl}/accounts/${accountId}`;
  const assetFormat = formatVersion || 'v5';
  const venueList = await fetchJson(`${accountUrl}/${assetFormat}.json`);
  const files = venueList[venueId].files; // mapping of asset "types" (spritesheet, style, badges, etc) to their URL

  const fetchedData = await fetchJson(files.venueData);
  const venueData = fetchedData[venueId];
  venueData.venueList = venueList;
  const contentStage = getContentStage(vconfig);
  venueData.files = contentStage // if a contentStage is defined, remap content URLs (see https://locuslabs.gitlab.io/webengine/webengine/contentStaging.html)
    ? remapEachURLForContentStage(venueData.category, files, contentStage, accountId, venueId)
    : files;

  return venueData
};

const buildStructures = (venueData) => {
  const { structureOrder, structures } = venueData;
  return structureOrder.map(structureId => {
    const structure = structures[structureId];
    // Was going to use this for selecting floors - but it seems floor bounds are incorrect? (was testing LAX/lax-msc-2 for example)
    // Object.values(structure.levels).forEach(level => (level.bounds = findBoundsOfCoordinates(level.boundsPolygon)))
    const bounds = findBoundsOfCoordinates(structure.boundsPolygon);
    return { ...structure, bounds }
  })
};

// If config.useDynamicUrlParams is true, allow changing even security-related parameters in
// the URL - else, only allow the changing of "end-user-changable" parameters, such as vid
// export function updateConfigWithUrlParams (config) {
//   const dlp = config.deepLinkProps
//   if (dlp) {
//     const venueId = dlp.vid ? dlp.vid : config.venueId
//     const assetStage = config.useDynamicUrlParams && dlp.stage ? dlp.stage : config.assetStage
//     const accountId = assetStage === 'alpha' ? 'A1VPTJKREFJWX5' : config.accountId
//     return { ...config, venueId, assetStage, accountId }
//   }
//   return config
// }

export { buildStructures, createFetchJson, createFetchText, getVenueDataFromUrls };
