var name = "SDK headless";
var plugins = {
	analytics: {
		platformSdk: "SDK",
		active: false
	},
	analytics2: {
		productName: "LocusMaps JS SDK",
		active: false,
		disableSending: false
	},
	clientAPI: {
	},
	dynamicPois: {
	},
	poiDataManager: {
	},
	sdkServer: {
	},
	searchService: {
	},
	venueDataLoader: {
		assetStage: "prod",
		formatVersion: "v5",
		availableLanguages: [
			{
				langCode: "en",
				assetSuffix: ""
			}
		]
	},
	wayfinder: {
		compareFindPaths: false
	}
};
var sdkHeadless = {
	name: name,
	plugins: plugins
};

export default sdkHeadless;
export { name, plugins };
