async function Auth (authConfig, app) {
  const auth = await (async () => {
    if (!authConfig || !authConfig.type)
      throw Error('No authentication type found in config')

    const resolveAuthModuleName = config => {
      switch (config.type) {
        case 'ms': return 'MSAuth'
        case 'cognito': return 'CognitoAuth'
        case 'google': return 'GoogleAuth'
        default: throw Error(`Unknown auth type: '${config.type}'`)
      }
    };

    return Promise.resolve(resolveAuthModuleName(authConfig))
      .then(moduleName => import(`./${moduleName}`))
      .then(module => module.default(authConfig.config, app))
  })();

  const getToken = async () => auth.getToken();

  const isLoggedIn = async () => auth.isLoggedIn();

  const federatedLogin = async () => auth.federatedLogin();

  const logout = () => auth.logout();

  const getAccountInfo = () => auth.getAccountInfo();

  const getType = () => authConfig.type;

  return { getToken, isLoggedIn, federatedLogin, logout, getAccountInfo, getType }
}

export default Auth;
