function pad (total, str) {
  while (str.length < total) { str += ' '; }
  return str
}

const getHelpHeader = () => pad(18, 'Command') + 'Arguments\n' + pad(18, '----------------') + '----------------\n';

/**
 *  Returns help on the passed command signature
 */
const getHelp = sig => pad(18, sig.command) +
  (sig.args ? sig.args.map(argob => `${argob.name} {${argob.type}} ${argob.optional ? ' (optional)' : ' (required)'}`).join(', ') : '');

/**
 *  Returns help on the passed command signatures
 */
const getHelpList = sigList => {
  return sigList.reduce((ret, sig) => `${ret}${getHelp(sig)}\n`, getHelpHeader())
};

export { getHelp, getHelpHeader, getHelpList };
